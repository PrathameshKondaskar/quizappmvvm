package com.example.quizappmvvm.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fitpeodemoapp.util.ApiState
import com.example.quizappmvvm.model.QuestionItem
import com.example.quizappmvvm.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: MainRepository) : ViewModel() {


    private val _questionsMutbaleLivedata = MutableLiveData<ApiState<List<QuestionItem>>>()
    val questionsLivedata: LiveData<ApiState<List<QuestionItem>>>
    get() = _questionsMutbaleLivedata

    private val _questionListMutbaleLiveData = MutableLiveData<List<QuestionItem>>()
    val questionListLiveData : LiveData<List<QuestionItem>>
    get() = _questionListMutbaleLiveData

    var questionHashmap : HashMap<String, String> ?= HashMap()



    fun getQuestions(){
        viewModelScope.launch {
            Log.d("TAG", "getQuestions: viewmodel")
            val result =repository.getQuestions()
            _questionsMutbaleLivedata.postValue(result)

          _questionListMutbaleLiveData.postValue(result.data!!)

        }
    }




}