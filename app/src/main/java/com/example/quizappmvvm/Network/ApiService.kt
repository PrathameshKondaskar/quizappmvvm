package com.example.quizappmvvm.Network

import com.example.quizappmvvm.model.QuestionItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @GET("api/v2/customerQuestionnaireMaster")
    suspend fun getQuestions():Response<List<QuestionItem>>

}