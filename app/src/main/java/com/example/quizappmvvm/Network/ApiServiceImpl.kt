package com.example.quizappmvvm.Network

import com.example.quizappmvvm.model.QuestionItem
import retrofit2.Response
import javax.inject.Inject

class ApiServiceImpl @Inject constructor(private val apiService: ApiService) {

    suspend fun getQuestions():Response<List<QuestionItem>> = apiService.getQuestions()

}