package com.example.quizappmvvm

import android.content.res.ColorStateList
import android.graphics.Color
import android.icu.number.IntegerWidth
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.core.view.marginStart
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.example.fitpeodemoapp.util.ApiState
import com.example.quizappmvvm.databinding.ActivityMainBinding
import com.example.quizappmvvm.model.QuestionItem

import com.example.quizappmvvm.viewmodel.MainViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var questionItemList :ArrayList<QuestionItem>
    var checkBoxList:ArrayList<AppCompatCheckBox> = arrayListOf()
    var positonArrayList : ArrayList<Int>  = arrayListOf()
    private val mainViewModel: MainViewModel by viewModels()
    var currentPosition = 0;
    var previousPosition =0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)
        setContentView(binding.root)
        Log.d("TAG", "onCreate: ")

        collectResultFromApi()
        positonArrayList.add(currentPosition);



    }

    private fun collectResultFromApi() {
        mainViewModel.getQuestions()

        mainViewModel.questionsLivedata.observe(this, Observer{

            when(it){

                is ApiState.Success->{

                    binding.progressBar.isVisible = false
                    questionItemList = it.data as ArrayList<QuestionItem>
                    Toast.makeText(this@MainActivity,"API Success "+questionItemList.get(0).question_english, Toast.LENGTH_SHORT).show()
                    mainViewModel.questionListLiveData.observe(this, {
                        addView(it.get(currentPosition))

                    })

                }

                is ApiState.Loading->{

                    binding.progressBar.isVisible = true
                }

                is ApiState.Failure->{

                    binding.progressBar.isVisible = false

                    Toast.makeText(this@MainActivity,"API Fail "+it.message, Toast.LENGTH_SHORT).show()

                    Log.d("TAG", "onCreate: ${it.message}")
                }

            }
        })

    }

    fun addView(questionItem: QuestionItem){
        binding.question.text ="Q." +questionItem.id+" "+questionItem.question_english
        binding.ParentLayout.removeAllViews()
        when(questionItem.input_type){


            "option"->{
                for (option in questionItem.option){

                    val checkBox = AppCompatCheckBox(this)
                    checkBox.setText(option.option_value)
                    checkBox.layoutParams = LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    checkBox.setPadding(20, 20, 20, 20)
                    checkBox.buttonTintList = ColorStateList.valueOf(resources.getColor(R.color.black))
                    checkBox.setTextColor(Color.BLACK)
                    binding.ParentLayout.addView(checkBox)
                    checkBoxList.add(checkBox)
                    checkBox.setOnClickListener{

                        for(checkbox in checkBoxList){
                            checkbox.isChecked = false
                            option.is_selected = false
                        }

                        checkBox.isChecked = true;
                        option.is_selected=true
                        currentPosition = Integer.valueOf(option.next_question_id)
                        Log.d("TAG", "addView: "+currentPosition)
                    }
                    checkBox.setOnCheckedChangeListener{buttonView ,isChecked->

                        option.is_selected=true
                        currentPosition = Integer.valueOf(option.next_question_id)

                    }
                    if(option.is_selected ){
                        checkBox.isChecked = true
                    }

                    /*mainViewModel.questionHashmap?.put(questionItem.id.toString(),option.id.toString())*/

                }

            }
            "radio"->{
                val radioGroup = RadioGroup(this)
                radioGroup.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                radioGroup.setPadding(20, 20, 20, 20)

                for(option in questionItem.option){

                    val radioButton = AppCompatRadioButton(this)
                    //setting height and width
                    radioButton.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    radioButton.setText(option.option_value) //setting text of first radio button
                    radioButton.setTextColor(Color.BLACK)
                    radioButton.buttonTintList = ColorStateList.valueOf(resources.getColor(R.color.black))
                    radioGroup.addView(radioButton)
                    currentPosition = Integer.valueOf(option.next_question_id);

                }
                binding.ParentLayout.addView(radioGroup)
            }
            "number"->{
                val editText = AppCompatEditText(this)
                editText.setHint("Give Ans")
                var layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                layoutParams.gravity= Gravity.CENTER
                editText.layoutParams = layoutParams
                editText.setPadding(40, 20, 20, 40)
                editText.setTextColor(Color.BLACK)
                ViewCompat.setBackgroundTintList(editText, ColorStateList.valueOf(resources.getColor(R.color.black)))
                editText.setHintTextColor(Color.GRAY)

                binding.ParentLayout.addView(editText)
                currentPosition = Integer.valueOf(questionItem.next_question_id)



            }

        }


    }

    fun onPrevious(view: View) {
        positonArrayList.removeAt(positonArrayList.size-1);
        previousPosition = positonArrayList.get(positonArrayList.size-1)
        addView(questionItemList.get(previousPosition))
    }
    fun onNext(view: View) {
        positonArrayList.add(currentPosition)
        addView(questionItemList.get(currentPosition))
    }
}