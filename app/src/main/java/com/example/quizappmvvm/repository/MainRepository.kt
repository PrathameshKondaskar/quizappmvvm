package com.example.quizappmvvm.repository

import android.annotation.SuppressLint
import android.util.Log
import com.example.fitpeodemoapp.util.ApiState
import com.example.quizappmvvm.Network.ApiServiceImpl
import com.example.quizappmvvm.model.QuestionItem
import javax.inject.Inject

class MainRepository  @Inject constructor(private val apiServiceImpl: ApiServiceImpl) {


    suspend fun getQuestions():ApiState<List<QuestionItem>>{
        val response = apiServiceImpl.getQuestions()
        Log.d("TAG", "getQuestions: response")
        return if(response.isSuccessful){
            val responseBody = response.body()
                if(responseBody != null){
                    Log.d("TAG", "getQuestions: success ")

                    ApiState.Success(responseBody)
                }else{
                    Log.d("TAG", "getQuestions: fail ")
                    ApiState.Failure("Something went wrong")
                }
        }else {
            Log.d("TAG", "getQuestions: fail fail ")
            ApiState.Failure("Something went wrong")
        }

    }


}